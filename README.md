# Control Centre
---

This app is based on and inspired by the [Control Center](https://badge.team/projects/control_center) app by [Niklas Roy](http://www.niklasroy.com/).

I took out many parts of the code that I thought weren't *that* necessary, in an attempt to make it a little less daunting, and more accessible. And I added a few features that I like.

#### removed features

* quick-/microlauncher
* animations
* 3d compass

#### added features

* flashlight and led toggle
* more extensive weather graphs

---

## How to use

In order to save power when not plugged in, the watch will only activate when you press a button or turn your wrist.
The display brightness will adjust to the ambient light level.

The buttons follow the general card10 semantics:
- **Top left:** home menu
- **Bottom left:** choose left
- **Bottom right:** choose right
- **Top right:** select / back

By pushing *left* or *right*, you can cycle through the different functions of the app. 
Pushing *select* will bring you to a dedicated screen of the highlighted function, or, if nothing is highlighted, toggle between flashlight on, leds on, and lights off.

---

## Control Centre Functions

### Time

When you have highlighted the current time and press *select*, you are brought to a screen, where you can 
- set the time
- set the date
- set an alarm


### Date

When you have highlighted the date and press *select*, an infinite monthly calendar is displayed. 
You can cycle through the months with the *left*/*right* buttons. Pushing *select* again brings you back to the main screen.


### Power

When you have highlighted the battery symbol and press *select*, a screen which shows all charge and battery related data in numbers is displayed.

Pushing *select* again brings you back to the main screen.


### Weather graph

In the center of the screen is a little graph with showing barometric pressure, humidity, and temperature within the last 24 hours. There will be no graph when you try out the app for the first time, as there are no recordings, yet, and the recordings only happen while *Control Centre* is running.

The legend below the graph shows relative humidity in percent, temperature in °C, and the difference between the highest and lowest pressure in the equivalent of metres (i.e. assuming that you were hiking on some small hills, not too far above the sea level).
The code for this calculation is taken from https://badge.team/projects/barometric_altimeter by Nubesik.

When you have highlighted the graph and press *select*, you are brought to a 32-hour log of pressure, temperature, and humidity, through which you can cycle with the left and right buttons. Pushing *select* brings you back to the main screen.


### Compass

A little compass is shown on the right side on the screen. It points to North (and it usually does a pretty good job). 
If the compass is not sure about its accuracy (e.g. if it detects a too strong magnetic field from nearby magnets) the outlining circle is dashed. If it's sure about it's accuracy, the outlining circle is drawn uninterrupted.


### Lights

Pressing the *select* button when you are on the main screen will toggle between the flashlight (as this does not come soldered to the card10 by default, the rocket leds will also turn on to give some feedback), all the other leds, and lights off.


### Notification LEDs

The orange rocket lights up when the activation gesture is detected. The left-most LED indicates loading and saving of settings (i.e. when you set an alarm), and the three right-most LEDs indicate loading and updating of the weather logs (color indicating which type is loaded/saved).


## Troubleshooting

### outdated cc_settings.json

Sometimes I change values, names, or general usage of the settings parameters without implementing a migration. So deleting the settings file `cc_settings.json` might be a first attempt to mitigate such a situation.

### corrupt log files

If something unexpected happens during updating/saving the log files, they might become unreadable for Control Centre (though, probably easily reparable for humans). If that happens delete the log files, or remove any lines that look corrupted from them.
